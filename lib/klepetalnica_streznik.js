var socketio = require('socket.io');
var io;
var stevilkaGosta = 1;
var vzdevkiGledeNaSocket = {};
var uporabljeniVzdevki = [];
var trenutniKanal = {};
var kanaliGesla = {};

exports.listen = function(streznik) {
  io = socketio.listen(streznik);
  io.set('log level', 1);
  io.sockets.on('connection', function (socket) {
    stevilkaGosta = dodeliVzdevekGostu(socket, stevilkaGosta, vzdevkiGledeNaSocket, uporabljeniVzdevki);
    pridruzitevKanalu(socket, 'Skedenj');
    obdelajPosredovanjeSporocila(socket, vzdevkiGledeNaSocket);
		obdelajZasebnoSporocilo(socket, vzdevkiGledeNaSocket);
    obdelajZahtevoZaSprememboVzdevka(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki);
    obdelajPridruzitevKanalu(socket);
    socket.on('kanali', function() {
      socket.emit('kanali', io.sockets.manager.rooms);
    });
    obdelajOdjavoUporabnika(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki);
  });
};

function dodeliVzdevekGostu(socket, stGosta, vzdevki, uporabljeniVzdevki) {
  var vzdevek = 'Gost' + stGosta;
  vzdevki[socket.id] = vzdevek;
  socket.emit('vzdevekSpremembaOdgovor', {
    uspesno: true,
    vzdevek: vzdevek
  });
  uporabljeniVzdevki.push(vzdevek);
  return stGosta + 1;
}

function pridruzitevKanalu(socket, kanal) {
  socket.join(kanal);
  trenutniKanal[socket.id] = kanal;
  socket.emit('pridruzitevOdgovor', {kanal: kanal});
  socket.broadcast.to(kanal).emit('sporocilo', {
    besedilo: vzdevkiGledeNaSocket[socket.id] + ' se je pridružil kanalu ' + kanal + '.'
  });

  var uporabnikiNaKanalu = io.sockets.clients(kanal);
	var seznamUporabnikov = [];
  if (uporabnikiNaKanalu.length > 1) {
    var uporabnikiNaKanaluPovzetek = 'Trenutni uporabniki na kanalu ' + kanal + ': ';
    for (var i in uporabnikiNaKanalu) {
      var uporabnikSocketId = uporabnikiNaKanalu[i].id;
      if (uporabnikSocketId != socket.id) {
        if (i > 0) {
          uporabnikiNaKanaluPovzetek += ', ';
        }
        uporabnikiNaKanaluPovzetek += vzdevkiGledeNaSocket[uporabnikSocketId];
				seznamUporabnikov.push(vzdevkiGledeNaSocket[uporabnikSocketId]);
      }
    }
    uporabnikiNaKanaluPovzetek += '.';
		seznamUporabnikov.push(vzdevkiGledeNaSocket[socket.id]);
    socket.emit('sporocilo', {besedilo: uporabnikiNaKanaluPovzetek});
  }
	else
	{
		seznamUporabnikov.push(vzdevkiGledeNaSocket[socket.id]);
	}
	socket.emit('seznam-uporabnikov', {seznam: seznamUporabnikov});
	socket.broadcast.to(trenutniKanal[socket.id]).emit('seznam-uporabnikov', {seznam: seznamUporabnikov});
}

function obdelajZahtevoZaSprememboVzdevka(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki) {

  socket.on('vzdevekSpremembaZahteva', function(vzdevek) {
    if (vzdevek.indexOf('Gost') == 0) {
      socket.emit('vzdevekSpremembaOdgovor', {
        uspesno: false,
        sporocilo: 'Vzdevki se ne morejo začeti z "Gost".'
      });
    } else {
      if (uporabljeniVzdevki.indexOf(vzdevek) == -1) {
        var prejsnjiVzdevek = vzdevkiGledeNaSocket[socket.id];
        var prejsnjiVzdevekIndeks = uporabljeniVzdevki.indexOf(prejsnjiVzdevek);
        uporabljeniVzdevki.push(vzdevek);
        vzdevkiGledeNaSocket[socket.id] = vzdevek;
        delete uporabljeniVzdevki[prejsnjiVzdevekIndeks];
        socket.emit('vzdevekSpremembaOdgovor', {
          uspesno: true,
          vzdevek: vzdevek
        });
        socket.broadcast.to(trenutniKanal[socket.id]).emit('sporocilo', {
          besedilo: prejsnjiVzdevek + ' se je preimenoval v ' + vzdevek + '.'
        });

				//posodobi vzdevke
				var seznamUporabnikov = [];
				var seznamSocketov = io.sockets.clients(trenutniKanal[socket.id]);
				
				for (var i in seznamSocketov)
				{
					seznamUporabnikov.push(vzdevkiGledeNaSocket[seznamSocketov[i].id]);
				}
				socket.broadcast.to(trenutniKanal[socket.id])
				.emit('seznam-uporabnikov', { seznam: seznamUporabnikov});
				socket.emit('seznam-uporabnikov', {seznam: seznamUporabnikov});

				
      } else {
        socket.emit('vzdevekSpremembaOdgovor', {
          uspesno: false,
          sporocilo: 'Vzdevek je že v uporabi.'
        });
      }
    }
  });
}

function obdelajPosredovanjeSporocila(socket) {
  socket.on('sporocilo', function (sporocilo) {
    socket.broadcast.to(sporocilo.kanal).emit('sporocilo', {
      besedilo: vzdevkiGledeNaSocket[socket.id] + ': ' + sporocilo.besedilo
    });
  });
}

function obdelajPridruzitevKanalu(socket) {
  socket.on('pridruzitevZahteva', function(kanal) {
		var stariKanal = trenutniKanal[socket.id];
		//preveri, ce je zraven geslo
		if((kanal.novKanal.match(/\"/g) || []).length == 4)
		{
			var kanalIme = kanal.novKanal.substring(kanal.novKanal.indexOf("\"")+1);
			var kanalGeslo = kanalIme;
			kanalIme = kanalIme.substring(0, kanalIme.indexOf("\""));
			kanalGeslo = kanalGeslo.substring(kanalGeslo.indexOf("\"")+1);
			kanalGeslo = kanalGeslo.substring(kanalGeslo.indexOf("\"")+1);
			kanalGeslo = kanalGeslo.substring(0, kanalGeslo.indexOf("\""));
			
			//preveri ce ze obstaja
			var obstaja = false;
			var freeObstaja = false;
			var pravilno = false;
			for(var i in kanaliGesla)
			{
				if(i == kanalIme)
				{
					obstaja = true;
					if(kanaliGesla[i] == kanalGeslo)
					{
						pravilno = true;
					}
				}
			}
			if(obstaja == false)
			{
				for(var i in trenutniKanal)
				{
					if(trenutniKanal[i] == kanalIme)
					{
						freeObstaja = true;
					}
				}
				if(freeObstaja == false)
				{
					//dodaj novega
					kanaliGesla[kanalIme] = kanalGeslo;
					pravilno = true;
				}
				else
				{
					socket.emit('sporocilo', {besedilo: "Izbrani kanal "+kanalIme+" je prosto dostopen in ne zahteva prijave z geslom, zato se prijavite z uporabo /pridruzitev <kanal> ali zahtevajte kreiranje kanala z drugim imenom."});
				}
			}
			
			if(pravilno)
			{
				socket.leave(trenutniKanal[socket.id]);
				//posodobi vzdevke (na starem kanalu)
				var seznamUporabnikov = [];
				var seznamSocketov = io.sockets.clients(stariKanal);
				
				for (var i in seznamSocketov)
				{
					if(seznamSocketov[i].id != socket.id)
					{
						seznamUporabnikov.push(vzdevkiGledeNaSocket[seznamSocketov[i].id]);
					}
				}
				socket.broadcast.to(stariKanal).emit('seznam-uporabnikov', { seznam: seznamUporabnikov});

    pridruzitevKanalu(socket, kanalIme);

			}
			else
			{
				if(freeObstaja == false)
				{
				socket.emit('sporocilo', {besedilo: "Pridruzitev v kanal "+kanalIme+" ni bila uspesna, ker je geslo napacno."});
				}
			}
		}
		else
		{
			//geslo ni podano
			var imaGeslo = false;
			for(var i in kanaliGesla)
			{
				if(i == kanal.novKanal)
				{
					imaGeslo = true;
				}
			}
			if(imaGeslo == false)
			{
				//obicajna prijava
				socket.leave(trenutniKanal[socket.id]);
				//posodobi vzdevke (na starem kanalu)
				var seznamUporabnikov = [];
				var seznamSocketov = io.sockets.clients(stariKanal);
				
				for (var i in seznamSocketov)
				{
					if(seznamSocketov[i].id != socket.id)
					{
						seznamUporabnikov.push(vzdevkiGledeNaSocket[seznamSocketov[i].id]);
					}
				}
				socket.broadcast.to(stariKanal).emit('seznam-uporabnikov', { seznam: seznamUporabnikov});
				pridruzitevKanalu(socket, kanal.novKanal);
			}
			else
			{
				socket.emit('sporocilo', {besedilo: "Ta kanal je zasciten z geslom, uporabite /pridruzitev \"kanal\" \"geslo\""});
			}
		}
  });
}

function obdelajOdjavoUporabnika(socket) {
  socket.on('odjava', function() {
		var stariKanal = trenutniKanal[socket.id];
    var vzdevekIndeks = uporabljeniVzdevki.indexOf(vzdevkiGledeNaSocket[socket.id]);
    delete uporabljeniVzdevki[vzdevekIndeks];
    delete vzdevkiGledeNaSocket[socket.id];
		//posodobi vzdevke (na starem kanalu)
				var seznamUporabnikov = [];
				var seznamSocketov = io.sockets.clients(stariKanal);
				
				for (var i in seznamSocketov)
				{
					if(seznamSocketov[i].id != socket.id)
					{
						seznamUporabnikov.push(vzdevkiGledeNaSocket[seznamSocketov[i].id]);
					}
				}
				socket.broadcast.to(stariKanal).emit('seznam-uporabnikov', { seznam: seznamUporabnikov});
  });
}

function obdelajZasebnoSporocilo(socket) 
{
	socket.on('zasebnoSporocilo', function(naslovnik, pm)
	{
		var naslovnikSocket;

		for (var i in vzdevkiGledeNaSocket)
		{
			if(vzdevkiGledeNaSocket[i] == naslovnik)
			{
				naslovnikSocket = io.sockets.socket(i);
			}
		}
		if(naslovnikSocket != undefined && naslovnikSocket.id != socket.id)
		{
			naslovnikSocket.emit('sporocilo', {besedilo: vzdevkiGledeNaSocket[socket.id]+"(zasebno): "+pm});
		}
		else
		{
			socket.emit('sporocilo', {besedilo: "Sporocila \""+pm+"\" uporabniku z vzdevkom \""+naslovnik+"\" ni bilo mogoce posredovati."});
		}
	});
	

}
