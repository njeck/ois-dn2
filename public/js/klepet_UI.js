function divElementEnostavniTekst(sporocilo) {
  sporocilo = sporocilo
  
  sporocilo = sporocilo
  
  //zamenjava samo na userjevem prikazu 
  sporocilo = sporocilo.replace(/\:\)/g,"<img src=https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/smiley.png>");
  sporocilo = sporocilo.replace(/\;\)/g,"<img src=https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/wink.png>");
  sporocilo = sporocilo.replace(/\(y\)/g,"<img src=https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/like.png>");
  sporocilo = sporocilo.replace(/\:\*/g,"<img src=https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/kiss.png>");
  sporocilo = sporocilo.replace(/\:\(/g,"<img src=https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/sad.png>");
  
  return $('<div style="font-weight: bold"></div>').html(sporocilo);
}


function divElementHtmlTekst(sporocilo) {
  return $('<div></div>').html('<i>' + sporocilo + '</i>');
}

function procesirajVnosUporabnika(klepetApp, socket) {
  var sporocilo = $('#poslji-sporocilo').val();
	//filtriraj ven vulgarne besede
	xmlhttp = new XMLHttpRequest();
	xmlhttp.open("GET", "swearWords.xml", false);
	xmlhttp.send();
	xmlDoc = xmlhttp.responseXML;
	
	


  var sistemskoSporocilo;

  if (sporocilo.charAt(0) == '/') {
    sistemskoSporocilo = klepetApp.procesirajUkaz(sporocilo);
    if (sistemskoSporocilo) {
      $('#sporocila').append(divElementHtmlTekst(sistemskoSporocilo));
    }
  } else {
		var words = xmlDoc.getElementsByTagName('word');
	for(var i=0; i < words.length; i++)
	{
		var word = words[i].childNodes[0].nodeValue;
		if(sporocilo.indexOf(word) > -1)
		{
			var replacement = "";
			for(var j=0; j < word.length; j++)
			{
				replacement = replacement.concat("*");
			}
			var regExp = new RegExp(word, 'gi');
			sporocilo = sporocilo.replace(regExp, replacement);
		}
	}
    klepetApp.posljiSporocilo($('#kanal').text(), sporocilo);
    $('#sporocila').append(divElementEnostavniTekst(sporocilo));
    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
  }

  $('#poslji-sporocilo').val('');
}

var socket = io.connect();

$(document).ready(function() {
  var klepetApp = new Klepet(socket);
	var kanal;
	var vzdevek;

  socket.on('vzdevekSpremembaOdgovor', function(rezultat) {
    var sporocilo;
    if (rezultat.uspesno) {
      sporocilo = 'Prijavljen si kot ' + rezultat.vzdevek + '.';
			vzdevek = rezultat.vzdevek;
			$('#kanal').text(vzdevek+"@"+kanal);
    } else {
      sporocilo = rezultat.sporocilo;
    }
    $('#sporocila').append(divElementHtmlTekst(sporocilo));
		//$('#seznam-uporabnikov').append(divElementHtmlTekst(rezultat.vzdevek));
  });

  socket.on('pridruzitevOdgovor', function(rezultat) {
		kanal = rezultat.kanal;
		$('#kanal').text(vzdevek+"@"+kanal);
		$('#sporocila').append(divElementHtmlTekst('Sprememba kanala.'));
  });

  socket.on('sporocilo', function (sporocilo) {
    sporocilo = sporocilo
    
    var content = sporocilo.besedilo;
    content = content.replace(/\:\)/g,"<img src=https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/smiley.png>");
    content = content.replace(/\;\)/g,"<img src=https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/wink.png>");
    content = content.replace(/\(y\)/g,"<img src=https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/like.png>");
    content = content.replace(/\:\*/g,"<img src=https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/kiss.png>");
    content = content.replace(/\:\(/g,"<img src=https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/sad.png>");
    
    var novElement = $('<div style="font-weight: bold"></div>').html(content);
    $('#sporocila').append(novElement);
  });
	socket.on('seznam-uporabnikov', function (uporabniki)
	{
		var seznamUporabnikov = uporabniki.seznam;
		$('#seznam-uporabnikov').empty();
		for(var i in seznamUporabnikov)
		{
			$('#seznam-uporabnikov').append(divElementEnostavniTekst(seznamUporabnikov[i]));
		}
	})

  socket.on('kanali', function(kanali) {
    $('#seznam-kanalov').empty();

    for(var kanal in kanali) {
      kanal = kanal.substring(1, kanal.length);
      if (kanal != '') {
        $('#seznam-kanalov').append(divElementEnostavniTekst(kanal));
      }
    }

    $('#seznam-kanalov div').click(function() {
      klepetApp.procesirajUkaz('/pridruzitev ' + $(this).text());
      $('#poslji-sporocilo').focus();
    });
  });

  setInterval(function() {
    socket.emit('kanali');
  }, 1000);

  $('#poslji-sporocilo').focus();

  $('#poslji-obrazec').submit(function() {
    procesirajVnosUporabnika(klepetApp, socket);
    return false;
  });
});
