var Klepet = function(socket) {
  this.socket = socket;
};

Klepet.prototype.posljiSporocilo = function(kanal, besedilo) {
  var sporocilo = {
    kanal: kanal,
    besedilo: besedilo
    
  };
  this.socket.emit('sporocilo', sporocilo);
};

Klepet.prototype.spremeniKanal = function(kanal) {
  this.socket.emit('pridruzitevZahteva', {
    novKanal: kanal
  });
};

Klepet.prototype.procesirajUkaz = function(ukaz) {
  var besede = ukaz.split(' ');
  ukaz = besede[0].substring(1, besede[0].length).toLowerCase();
  var sporocilo = false;

  switch(ukaz) {
    case 'pridruzitev':
      besede.shift();
      var kanal = besede.join(' ');
      this.spremeniKanal(kanal);
      break;
    case 'vzdevek':
      besede.shift();
      var vzdevek = besede.join(' ');
      this.socket.emit('vzdevekSpremembaZahteva', vzdevek);
      break;
		case 'zasebno':
			besede.shift();
			var tekst = besede.join(' ');
			if((tekst.match(/\"/g) || []).length == 4)
			{
				var naslovnik = tekst.substring(tekst.indexOf("\"")+1);
				var pm = tekst.substring(tekst.indexOf("\"")+1);
				naslovnik = naslovnik.substring(0, naslovnik.indexOf("\""));
				pm = pm.substring(pm.indexOf("\"")+1); 
				pm = pm.substring(pm.indexOf("\"")+1); 
				pm = pm.substring(0, pm.indexOf("\""))
				//filter vulgarnih besed
				var xmlhttp = new XMLHttpRequest();
				xmlhttp.open("GET", "swearWords.xml", false);
				xmlhttp.send();
				var xmlDoc = xmlhttp.responseXML;
				var words = xmlDoc.getElementsByTagName('word');
				for(var i=0; i<words.length; i++)
				{
					var word = words[i].childNodes[0].nodeValue;
					if(pm.indexOf(word) > -1)
					{
						var replacement = "";
						for(var j = 0; j<word.length; j++)
						{
							replacement = replacement.concat("*");
						}
						var regExp = new RegExp(word, 'gi');
						pm = pm.replace(regExp, replacement);
						pm = pm.replace(/\:\)/g,"<img src=https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/smiley.png>");

						pm = pm.replace(/\;\)/g,"<img src=https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/wink.png>");

						pm = pm.replace(/\(y\)/g,"<img src=https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/like.png>");

						pm = pm.replace(/\:\*/g,"<img src=https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/kiss.png>");

						pm = pm.replace(/\:\(/g,"<img src=https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/sad.png>");


					}
				}
				
				sporocilo = "(zasebno za "+naslovnik+"): "+pm;
				this.socket.emit('zasebnoSporocilo', naslovnik, pm);
			}
			else
			{
				sporocilo = 'Napacen format.';
			}
			break;
    default:
      sporocilo = 'Neznan ukaz.';
      break;
  };

  return sporocilo;
};
